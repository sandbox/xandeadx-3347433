<?php

namespace Drupal\dadata_api_wrapper\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure DaData API Wrapper settings for this site.
 */
class DadataApiSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'dadata_api_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['dadata_api_wrapper.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $dadata_api_config = $this->config('dadata_api_wrapper.settings');

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key'),
      '#default_value' => $dadata_api_config->get('api_key'),
    ];

    $form['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret key'),
      '#default_value' => $dadata_api_config->get('secret_key'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('dadata_api_wrapper.settings')
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('secret_key', $form_state->getValue('secret_key'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
