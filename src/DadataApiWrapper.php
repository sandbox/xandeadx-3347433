<?php

namespace Drupal\dadata_api_wrapper;

use Dadata\DadataClient;
use Dadata\Settings;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

class DadataApiWrapper extends DadataClient {

  protected CacheBackendInterface $cacheBackend;

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory, CacheBackendInterface $cache_backend) {
    $this->cacheBackend = $cache_backend;
    $dadata_api_config = $config_factory->get('dadata_api_wrapper.settings');
    parent::__construct($dadata_api_config->get('api_key'), $dadata_api_config->get('secret_key'));
  }

  /**
   * {@inheritDoc}
   */
  public function suggest($name, $query, $count = Settings::SUGGESTION_COUNT, $kwargs = [], bool $use_cache = TRUE) {
    $named_func_args = get_defined_vars();
    unset($named_func_args['use_cache']);
    $cache_key = __FUNCTION__ . ':' . md5(json_encode($named_func_args));

    if ($use_cache && ($cache = $this->cacheBackend->get($cache_key))) {
      $result = $cache->data['result'];
    }
    else {
      if ($result = parent::suggest($name, $query, $count, $kwargs)) {
        $this->cacheBackend->set($cache_key, ['arguments' => $named_func_args, 'result' => $result], strtotime('+1 month'));
      }
    }

    return $result;
  }

}
